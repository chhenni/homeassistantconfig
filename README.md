#Konfig for Homeassistant i Gullhaugveien 112

## Oppsett for å få git til å fungere rett:
https://community.home-assistant.io/t/home-assistant-config-editing-with-git-workflow-after-using-the-all-in-one-installer/1340



## Oppdateringer
    sudo systemctl stop home-assistant@homeassistant.service
    sudo -u homeassistant -H -s 
    source /srv/homeassistant/bin/activate
    pip3 install --upgrade homeassistant
    exit
    sudo systemctl restart home-assistant@homeassistant.service

## Aktivering av virtual environment
    sudo -u homeassistant -H -s
    source /srv/homeassistant/bin/activate


## Remote url
ssh://pi@192.168.0.8/home/pi/hass_updates_pending/hass_configs.git