import os
import sys
import json
import subprocess


message = sys.argv[1]
jmessage = json.loads(message)

if("rows" in jmessage):
    if("row" not in jmessage):
        if(len(jmessage["rows"]) > 1):
            jmessage["row"] = jmessage["rows"][1]
        else:
        	jmessage["row"] = jmessage["rows"][0]


type = jmessage["model"]
if("Silverline-Doorbell" in type):
    type = "Silverline-Doorbell"
    jmessage["rows"] = None
    sensorId = str(jmessage["row"]["channel"])
elif(type == "Prologue sensor"):
    sensorId = str(jmessage["id"]) + "/" + str(jmessage["channel"])
elif(type == "Proove"):
    sensorId = str(jmessage["id"]) + "/" + str(jmessage["group"]) + "/" + str(jmessage["unit"])
else:
    print("Unknown device")
    print(message)
    exit(0)

topic = "radio/443/" + type + "/" + sensorId



nextCommand = ["mosquitto_pub", "-V", "mqttv311", "-u", "homeassistant", "-P", "ghv112mqtt", "-t", topic, "-m", json.dumps(jmessage)]



process = subprocess.Popen(nextCommand, stdout=subprocess.PIPE)
process.wait()
for line in process.stdout:
    print(line)

#sudo rtl_433 -F json | xargs -I{} -r -n1 mosquitto_pub -V mqttv311 -u homeassistant -P ghv112mqtt -t "radio/443" -m "{}"


#sudo rtl_433 -F json -C si | xargs -I{} -r -d'\n' -n1 python /home/homeassistant/.homeassistant/rtl433ToMQTT.py "{}"


